# Webform HTML notes


## Introduction


Use case
Let's say a user's account has the field of the "Date" type to collect a user's
birthday. And let's assume we need to create a view to display all users that
came of age.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/webform_html_notes).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/webform_html_notes).

## Requirements

The [Webform](https://www.drupal.org/project/webform) module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module does not require configuration.


## How to Use

Open the "Notes" form and select a desired text format from the "Text format"
list.


## Maintainers

- Andrey Vitushkin (wombatbuddy) - https://www.drupal.org/u/wombatbuddy
